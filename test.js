const zwaveTestPoint = process.argv[2]
if(!zwaveTestPoint)return console.log(`Missing Test Endpoint. Example : http://local:12345678@localhost:8083`)
const zWaveAPI = require('./index.js')
console.log(zWaveAPI)
const {
    apiRequest,
    updateParameter,
    createParameter,
    getStatus,
    restartController,
    getDevice,
    updateDevice,
    sendCommandToDevice,
    getDeviceHistory,
    getLocation,
    updateLocation,
    deleteLocation,
    createLocation,
    getProfile,
    updateProfile,
    deleteProfile,
    createProfile,
    notifications,
    getNotification,
    updateNotification,
    startPollingNotifications,
    stopPollingNotifications,
    resetPollingNotificationsLastTimeCheck,
    getNamespace,
    getInstance,
    updateInstance,
    deleteInstance,
    createInstance,
} = zWaveAPI(zwaveTestPoint,true)

console.log('Z-Wave Test Point',zwaveTestPoint)

async function doTest(){
    let response = await getStatus()
    console.log(response)
    console.log('getStatus ^^^')
    response = await getDevice()
    console.log(response)
    console.log('getDevice ^^^')
    response = await getProfile()
    console.log(response)
    console.log('getProfile ^^^')
    response = await getLocation()
    console.log(response)
    console.log('getLocation ^^^')
    response = await getNamespace()
    console.log(response)
    console.log('getNamespace ^^^')
    response = await getInstance()
    console.log(response)
    console.log('getInstance ^^^')
    response = await getNotification(null,1387884437)
    console.log(response)
    console.log('getNotification ^^^')
    const startTimeForPolling = (new Date().getTime() / 1000)
    const startTimeForPolling4HoursAgo = startTimeForPolling - 60 * 60 * 24
    console.log('Beginning Polling for Notifications every 5 seconds...',startTimeForPolling,new Date(startTimeForPolling4HoursAgo * 1000))
    resetPollingNotificationsLastTimeCheck(startTimeForPolling4HoursAgo)
    startPollingNotifications((list) => {
        console.log(list.length)
    })
}
doTest()
