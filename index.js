// based on : https://zwayhomeautomation.docs.apiary.io/
// `sinceTime` is in SECONDS (new Date().getTime() / 1000)
// `putData` is always an object.
const fetch = require('node-fetch');

module.exports = (zWaveHost, debug) => {
    function timeInSeconds() {
        return parseInt((new Date().getTime()) / 1000);
    }

    function httpRequest(path) {
        const host = zWaveHost || `http://local:12345678@localhost:8083`;
        const fetchUrl = `${host}/${path}`;
        return fetch(fetchUrl);
    }

    async function getImage(path) {
        return await httpRequest(`storage/img/${path}`);
    }

    async function apiRequest(apiPoint, options = {}) {
        const host = zWaveHost || `http://local:12345678@localhost:8083`;
        const fetchUrl = `${host}/ZAutomation/api/v1/${apiPoint}`;
        const requestOptions = {
            method: options.requestType || 'GET',
            headers: { 'Content-Type': 'application/json' },
            body: options.data ? JSON.stringify(options.data) : undefined,
        };

        try {
            const response = await fetch(fetchUrl, requestOptions);
            let parsedBody = await response.text();

            try {
                parsedBody = JSON.parse(parsedBody).data;
            } catch (err) {
                console.log('Z-Wave apiRequest err', err);
                console.log('Z-Wave apiRequest body', parsedBody);
            }

            if (debug) console.log(fetchUrl, parsedBody);
            return parsedBody;
        } catch (error) {
            console.log('Z-Wave apiRequest fetch error', error);
            throw error;
        }
    }

    async function updateParameter(param, paramId, putData) {
        return await apiRequest(`${param}/${paramId}`, {
            requestType: 'PUT',
            data: putData,
        });
    }

    async function createParameter(param, paramId, putData) {
        return await apiRequest(`${param}/${paramId}`, {
            requestType: 'POST',
            data: putData,
        });
    }

    async function deleteParameter(param, paramId) {
        return await apiRequest(`${param}/${paramId}`, {
            requestType: 'DELETE',
        });
    }

    async function getStatus() {
        return await apiRequest(`status`);
    }

    async function restartController() {
        return await apiRequest(`restart`);
    }

    async function getDevice(deviceId, sinceTime) {
        return await apiRequest(`devices${deviceId ? `/${deviceId}` : ''}${!deviceId && sinceTime ? `?since=${sinceTime}` : ''}`);
    }

    async function updateDevice(deviceId, putData) {
        return await updateParameter('devices', deviceId, putData);
    }

    async function sendCommandToDevice(deviceId, command) {
        return await apiRequest(`devices/${deviceId}/command/${command}`);
    }

    async function getDeviceHistory(deviceId, sinceTime) {
        return await apiRequest(`history${deviceId ? `/${deviceId}` : ''}${!deviceId && sinceTime ? `?since=${sinceTime}` : ''}`);
    }

    async function getLocation(locationId) {
        return await apiRequest(`locations${locationId ? `/${locationId}` : ''}`);
    }

    async function updateLocation(locationId, putData) {
        return await updateParameter('locations', locationId, putData);
    }

    async function deleteLocation(locationId) {
        return await deleteParameter('locations', locationId);
    }

    async function createLocation(title, iconUrl) {
        return await createParameter('locations', {
            title: title,
            icon: iconUrl,
        });
    }

    async function getProfile(profileId) {
        return await apiRequest(`profiles${profileId ? `/${profileId}` : ''}`);
    }

    async function updateProfile(profileId, putData) {
        return await updateParameter('profiles', profileId, putData);
    }

    async function deleteProfile(profileId) {
        return await deleteParameter('profiles', profileId);
    }

    async function createProfile(title, iconUrl) {
        return await createParameter('profiles', {
            title: title,
            icon: iconUrl,
        });
    }

    async function getNotification(noteId, sinceTime) {
        return await apiRequest(`notifications${noteId ? `/${noteId}` : ''}${!noteId && sinceTime ? `?since=${sinceTime}` : ''}`);
    }

    async function updateNotification(noteId, putData) {
        return await updateParameter('notifications', noteId, putData);
    }

    const notifications = {};
    notifications.lastTimeCheck = new Date() - 120000;
    notifications.poller = null;
    notifications.mostRecent = null;

    async function startPollingNotifications(callbackOnFound, pollInterval) {
        clearInterval(notifications.poller);
        notifications.poller = setInterval(async () => {
            const response = await apiRequest(`notifications?since=${notifications.lastTimeCheck}`);
            const allRecentNotifications = response.notifications;
            notifications.mostRecent = allRecentNotifications;
            if (allRecentNotifications.length > 0) {
                callbackOnFound(allRecentNotifications);
            }
            notifications.lastTimeCheck = new Date();
            if (debug) console.log(response);
        }, pollInterval || 5000);
    }

    async function stopPollingNotifications() {
        clearInterval(notifications.poller);
    }

    async function resetPollingNotificationsLastTimeCheck(newTime) {
        notifications.lastTimeCheck = parseInt(newTime) || (new Date() - 120000);
    }

    async function getNamespace(namespaceId) {
        return await apiRequest(`namespaces${namespaceId ? `/${namespaceId}` : ''}`);
    }

    async function getInstance(instanceId) {
        return await apiRequest(`instances${instanceId ? `/${instanceId}` : ''}`);
    }

    async function updateInstance(instanceId, putData) {
        return await updateParameter('instances', instanceId, {
            error: null,
            data: putData,
            code: 200,
            message: null,
        });
    }

    async function deleteInstance(instanceId) {
        return await deleteParameter('instances', instanceId);
    }

    async function createInstance(postData) {
        return await createParameter('instances', {
            error: null,
            data: postData,
            code: 200,
            message: null,
        });
    }

    return {
        apiRequest,
        updateParameter,
        createParameter,
        timeInSeconds,
        getStatus,
        restartController,
        getDevice,
        updateDevice,
        sendCommandToDevice,
        getDeviceHistory,
        getLocation,
        updateLocation,
        deleteLocation,
        createLocation,
        getProfile,
        updateProfile,
        deleteProfile,
        createProfile,
        notifications,
        getNotification,
        updateNotification,
        startPollingNotifications,
        stopPollingNotifications,
        resetPollingNotificationsLastTimeCheck,
        getNamespace,
        getInstance,
        updateInstance,
        deleteInstance,
        createInstance,
        httpRequest,
        getImage,
    };
};
