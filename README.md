## Unofficial Z-Wave Node.js Wrapper (ZAutomation)
by Moe Alam, Shinobi Systems
https://shinobi.video

*This module is only complete to the necessary functions for the project intended at present. Pull requests are welcome to make it more complete*

> Wrapper is based on https://zwayhomeautomation.docs.apiary.io/. All functions are async. Responses will attempt to automatically navigate into the `data` object if it exists within the response.

## Get Started

Run in project folder.
```
npm install shinobi-zwave
```

Add to your code.
```
const zwaveTestPoint = 'http://local:12345678@localhost:8083';
const zWaveAPI = require('shinobi-zwave');
const {
    apiRequest,
    updateParameter,
    createParameter,
    getStatus,
    restartController,
    getDevice,
    updateDevice,
    sendCommandToDevice,
    getDeviceHistory,
    getLocation,
    updateLocation,
    deleteLocation,
    createLocation,
    getProfile,
    updateProfile,
    deleteProfile,
    createProfile,
    notifications,
    getNotification,
    updateNotification,
    startPollingNotifications,
    stopPollingNotifications,
    getNamespace,
    getInstance,
    updateInstance,
    deleteInstance,
    createInstance,
} = zWaveAPI(zwaveTestPoint);
```

## My Really Bad Docs

> Flip back and forth with their docs https://zwayhomeautomation.docs.apiary.io/ to make sense of the awful docs below.

> All functions return a Promise!

**System**
```
getStatus()
restartController()
```

**Device**
```
getDevice(deviceId,sinceTime) // parameters can be left undefined to get all, getDevice().
updateDevice(deviceId,putData)
sendCommandToDevice(deviceId,command)
getDeviceHistory(deviceId,sinceTime)
```

**Location**
```
getLocation(locationId) // parameters can be left undefined to get all, getLocation().
updateLocation(locationId,putData)
deleteLocation(locationId)
createLocation(title,iconUrl)
```

**Profile**
```
getProfile(profileId) // parameters can be left undefined to get all, getProfile().
updateProfile(profileId,putData)
deleteProfile(profileId)
createProfile(title,iconUrl)
```

**Notification**
```
getNotification(noteId,sinceTime) // parameters can be left undefined to get all, getNotification().
updateNotification(noteId,putData)
startPollingNotifications(callbackOnFound,pollInterval)
stopPollingNotifications()
```

**Instance**
```
getNamespace(namespaceId) // parameters can be left undefined to get all, getNamespace().
getInstance(instanceId)
updateInstance(instanceId,putData)
deleteInstance(instanceId)
createInstance(postData)
```

**Namespace**
```
getNamespace(namespaceId)
```
